import 'package:flutter/material.dart';
import 'package:first_app/gradient_container.dart';

void main() {
  runApp(
    const MaterialApp(
      home: Scaffold(
        body: GradientContainer(
          Color.fromARGB(255, 81, 36, 158),
          Color.fromARGB(255, 177, 137, 245),
        ),
      ),
    ),
  );
}
